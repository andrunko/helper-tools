#!/bin/sh
# This scripts calls update_test_binaries in a loop
# passing it each line to test-repositories.txt

cat test-repositories.txt | while read repository; do
	./update_test_binaries.sh -r ${repository} -l 18.06 -b master
done
