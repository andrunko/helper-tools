#!/bin/bash

set -ex

branch=master

usage()
{
	echo "usage: $0 -l RELEASE_VERSION -r REPOSITORY [-b BRANCH] | [-h]"
	echo
	echo "    -l, --release RELEASE_VERSION  The release version to download packages/binaries from (see https://repositories.apertis.org/apertis/dists/)"
	echo "    -r, --repository REPOSITORY    The name of the repository to update (under https://gitlab.apertis.org/tests)"
	echo "    -b, --branch BRANCH            The branch in the repository to push the changes to (default: master)"
	echo "    -w, --workdir WORKDIR          The workdir (default: current dir)"
	echo "    -h, --help                     Display this help and exit"
}

while [ "$1" != "" ]; do
	case $1 in
		-l | --release )	shift
							release=$1
							;;
		-r | --repository )	shift
							repository=$1
							;;
		-b | --branch )		shift
							branch=$1
							;;
		-w | --workdir )	shift
							workdir=$1
							;;
		-h | --help )		usage
							exit 1
							;;
	esac
	shift
done

if [ -z "$release" -o -z "$repository" ] ; then
	usage
	exit 1
fi

if [ -n "${workdir}" ]; then
	mkdir -p "${workdir}"
	pushd "${workdir}"
fi

RELEASE=${release}
BRANCH=${branch}
TEST=${repository}
SOURCES="target development"
ARCHS="amd64 armhf arm64"
PACKAGES=""
BINARIES=""

download_packages_description() {
	for SOURCE in ${SOURCES} ; do
		[ -f Packages.${ARCH}.${SOURCE} ] && continue
		wget https://repositories.apertis.org/apertis/dists/${RELEASE}/${SOURCE}/binary-${ARCH}/Packages -O Packages.${ARCH}.${SOURCE}
	done
}

get_package() {
	local PACKAGE=$1
	for SOURCE in ${SOURCES} ; do
		FILENAME=$(sed -n -e "/^Package: ${PACKAGE}$/,/^Description:/  s/Filename: \(.*\)/\1/p" Packages.${ARCH}.${SOURCE})
		BASEFILENAME=$(basename "${FILENAME}")
		if [ -n "${FILENAME}" -a ! -f "${BASEFILENAME}" ]; then
			wget https://repositories.apertis.org/apertis/${FILENAME}
		fi
	done
}

download_extract_packages () {
	local PACKAGE=""
	mkdir -p ${EXTRACT}

	while read -r PACKAGE FILE ; do
		get_package ${PACKAGE}
		dpkg --extract ${PACKAGE}_*_${ARCH}.deb ${EXTRACT}
	done < "${TEST}/external-binaries.cfg"
}

clone_test_repository () {
	# Do not clone project if already cloned
	[ -d "${TEST}" ] || git clone -b ${BRANCH} --depth 1 "ssh://git@gitlab.apertis.org/tests/${TEST}.git"
}

test_repository_requires_binaries () {
	[ -f "${TEST}/external-binaries.cfg" ] || return 1
	return 0
}

update_test_repository () {
	while read -r PACKAGE FILE ; do
		local GIT="git -C ${TEST}"
		local DIR="bin"
		local OUTDIR="${ARCH}/${DIR}"

		mkdir -p ${TEST}/${OUTDIR}
		cp ${EXTRACT}/${FILE} ${TEST}/${OUTDIR}

		${GIT} add ${OUTDIR}/$(basename ${FILE})

		# Prepare the commit message (with a new line)
		echo $(ls ${PACKAGE}_*_${ARCH}.deb)" ${OUTDIR}/$(basename ${FILE})" >> ${TEST}-commit-msg.txt
	done < "${TEST}/external-binaries.cfg"
}

commit_all () {
	local GIT="git -C ${TEST}"
	local CHANGES=$(${GIT} diff-index --name-only HEAD --)

	if [ -n "${CHANGES}" ]; then
		${GIT} config --local "user.email" "jenkins@apertis.org"
		${GIT} config --local "user.name" "Apertis jenkins"
		${GIT} commit -s -F ../${TEST}-commit-msg.txt
		${GIT} push origin HEAD:${BRANCH}
	fi
}

clone_test_repository ${repository} || exit 1
if ! test_repository_requires_binaries ${repository}; then
	echo "Nothing to do for ${repository}"
	exit 0
fi

echo "Binary update ${TEST} $(date +%Y%M%d)" > ${TEST}-commit-msg.txt
echo >> ${TEST}-commit-msg.txt

for ARCH in ${ARCHS} ; do
	EXTRACT="extract_${ARCH}"

	download_packages_description

	download_extract_packages

	update_test_repository
done

commit_all

echo "Done"
